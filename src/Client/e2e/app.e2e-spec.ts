import { AngularSamPage } from './app.po';

describe('angular-sam App', function() {
  let page: AngularSamPage;

  beforeEach(() => {
    page = new AngularSamPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
