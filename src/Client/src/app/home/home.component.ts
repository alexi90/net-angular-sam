import { Component, OnInit } from '@angular/core';
import { TestService, Person } from '../services/test.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [TestService]
})
export class HomeComponent implements OnInit {
  private data: String = "";
  private persons: Person[] = [];
  constructor(public testService:TestService) { }
  getData(): void {
    this.testService
      .loadData()
      .then(res => this.persons = res);
  }
 
  ngOnInit() { this.getData() }
}
